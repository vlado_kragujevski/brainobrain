using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Bootstrap31.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues = null, object htmlAttributes = null)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var htmlAttributesDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            var anchorTag = new TagBuilder("a"){
                InnerHtml = linkText
            };
            anchorTag.MergeAttribute("href",
                routeValues == null
                    ? urlHelper.Action(actionName, controllerName)
                    : urlHelper.Action(actionName, controllerName, routeValues));
            anchorTag.MergeAttributes(htmlAttributesDictionary);

            var builder = new TagBuilder("li")
                              {
                                  InnerHtml = anchorTag.ToString()
                              };

            if (controllerName == currentController && actionName == currentAction)
                builder.AddCssClass("active");

            return new MvcHtmlString(builder.ToString());
        }

    
    }
}
