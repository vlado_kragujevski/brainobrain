using System.Web.Mvc;

namespace Bootstrap31.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Program()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            return View();
        }

        public ActionResult News()
        {
            return View();
        }

        public ActionResult Questions()
        {
            return View();
        }

        public ActionResult Location()
        {
            return View();
        }
    }
}
